window.addEventListener("load", function() {
	// Stran naložena
		
	// Posodobi opomnike
	var posodobiOpomnike = function() {
		var opomniki = document.querySelectorAll(".opomnik");
		
		for (var i = 0; i < opomniki.length; i++) {
			var opomnik = opomniki[i];
			var casovnik = opomnik.querySelector("span");
			var cas = parseInt(casovnik.innerHTML, 10);
	
			// TODO:
			// - če je čas enak 0, izpiši opozorilo
			// - sicer zmanjšaj čas za 1 in nastavi novo vrednost v časovniku
			if(cas == 0){
				var o = opomnik.querySelector(".naziv_opomnika").innerHTML;
				alert("Opomnik!\n\nZadolžitev "+o+" je potekla!");
				document.getElementById("opomniki").removeChild(opomnik);
			}
			else{
				casovnik.innerHTML=cas-1;
				
			}
		}
	};
	
	setInterval(posodobiOpomnike, 1000);
	
});